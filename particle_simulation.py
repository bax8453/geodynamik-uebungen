from random import random

from sklearn.metrics import mean_poisson_deviance
#from geodyn import simulation
from solarsystem import Particle
import numpy as np
from numpy import empty, sqrt, sign
import matplotlib.pyplot as plt
import random
from pprint import pprint
from typing import Tuple, List
from time import time



'''
To-Do:
1. Teilchen die sich zu weit entfernen, sollen ignoriert werden.
3. Variabler Zeitschritt (?).
4. Leapfrog Integrator integrieren (?).
5. Stöße fixen.
6. Anderes Stoßkonzept (?).
'''

# Preparation steps

random.seed(5)

def timer(func):
    '''
    Function decorator for measuring computation time. Add with @ prior to function definition. (See simulation)
    Prints out time elapsed during function computation. 
    Be careful with plotting functions! While the figure is open the timer is running.
    '''
    def inner(*args, **kwargs):
        start=time()
        func(*args, **kwargs)
        end=time()
        print(func.__name__ + ': ' + str(round(end-start,3)) + ' secs')
    return inner



# Declaration of classes for particle systems and single particles

class Cluster():
    '''
    The Cluster Class manages all necessary steps for the simulation of all particles. It is initiated with a list of all particles.
    Methods in this Class span from background simulation to on-screen plotting of trajectories. For different plots, please consider adding
    a new method. (e.g. plot3d)
    
    Args:
        timesteps=2000: timesteps to be simulated
        bodies=[]:      list of bodies. Supposed to contain Instances of `Particle`
        
    Returns:
        Cluster object
    '''


    def __init__(self, timesteps=2000, bodies=[]) -> None:
        self.name = 'Solar System'
        self.bodies = bodies
        self.timesteps = timesteps
        self.all_collisions = []


    @timer
    def create_bodies(self, num_bodies=100, min_mass=100, max_mass=1000, max_radius=1000, max_vel=0, s='2D') -> None:
        '''
        Initiate all bodies with random mass, position and velocity. Limits for respective values are given as arguments.
        
        Args:
            num_bodies: Number of bodies in simulation.
            min_mass:   Minimum mass of each body. [kg]
            max_mass:   Maximum mass of each body. [kg]
            max_radius: Maximal start distance from center. [m]
            max_vel:    Maximal absolut start velocity. Direction is random. [m/s]!
        
        Returns: 
            None
        '''
        # Farbe der Körper
        color = ['orange', 'brown', 'magenta', 'blue', 'red', 'cyan', 'yellow', 'pink', 'blue', 'gray', 'violet', 'black',
             'grey', 'green']

        # 1/7 der Körper hat eine schwerere Masse und 6/7 eine leichtere Masse
        min_mass_large = (max_mass - min_mass) * 0.7
        max_mass_large = max_mass
        min_mass_small = min_mass
        max_mass_small = (max_mass - min_mass) * 0.6

        # Wird benötigt als Rauschen für die Geschwindigkeit der Körper
        min_vel = max_vel * 10 ** (-10)
        for i in range(num_bodies):
            if i <= int(1 / 7 * num_bodies):
                mass = random.uniform(min_mass_large, max_mass_large)
            else:
                mass = random.uniform(min_mass_small, max_mass_small)

            # Die Geschwindigkeit der Körper hängt ab von der Masse plus ein gewisser Rauschanteil
            v = max_vel / mass + random.gauss(mu=0, sigma=min_vel)

            # Aufteilen der gesamten Geschwindigkeit v in die x, y und z Koordinate für 2D und 3D
            if s == '2D':
                v_x = (-1)**random.randrange(2) * random.uniform(0, 1)  # v hat gleiche Richtung wie r
                v_y = (-1)**random.randrange(2) * (1 - abs(v_x))
                v_z = 0
            else:
                v_x = random.uniform(0, 1)
                v_y = random.uniform(0, (1 - abs(v_x)))
                v_z = (1 - abs(v_x) - abs(v_y))

            # Zuteilung der Eigenschaften zum Körper
            # Der Ort im Raum ist gleichmäßig, aber zufällig verteilt
            self.bodies.append(Particle(
                i,
                random.uniform(-max_radius, max_radius),
                random.uniform(-max_radius, max_radius),
                random.uniform(-max_radius, max_radius),
                v_x * v,
                v_y * v,
                v_z * v,
                mass,
                color[i % 13],
                self.timesteps))



        # for i in range(num_bodies):
        #     self.bodies.append(Particle(
        #         i,
        #         random.uniform(-max_radius, max_radius),
        #         random.uniform(-max_radius, max_radius),
        #         random.uniform(-max_radius, max_radius),
        #         random.gauss(mu=0, sigma=max_vel/2),
        #         random.gauss(mu=0, sigma=max_vel/2),
        #         random.gauss(mu=0, sigma=max_vel/2),
        #         random.uniform(min_mass, max_mass),
        #         color[i % 13],
        #         self.timesteps))




        # # 1/7 der Körper hat eine schwerere Masse und 6/7 eine leichtere Masse
        # min_mass_large = (max_mass - min_mass) * 0.7
        # max_mass_large = max_mass
        # min_mass_small = min_mass
        # max_mass_small = (max_mass - min_mass) * 0.6
        #
        # min_vel = max_vel * 10**(-10)
        # for i in range(num_bodies):
        #     if i <= int(1/7 * num_bodies):
        #         mass = random.uniform(min_mass_large, max_mass_large)
        #     else:
        #         mass = random.uniform(min_mass_small, max_mass_small)
        #     r = max_radius/mass * 10 + random.gauss(mu=0, sigma=max_radius/100)
        #     v = max_vel / mass + random.gauss(mu=0, sigma=min_vel)
        #
        #     # Vorzeichen von r_x, Größe von r_x und gaußsches Rauschen
        #     if s == '2D':
        #         r_x = (-1)**random.randrange(2) * random.uniform(0, 1)
        #         r_y = (-1)**random.randrange(2) * (1 - r_x)
        #         r_z = 0
        #         v_x = r_x  # v hat gleiche Richtung wie r
        #         v_y = r_y
        #         v_z = r_z
        #     else:
        #         r_x = random.uniform(0, 1)
        #         r_y = random.uniform(0, (1 - r_x))
        #         r_z = (1 - r_x - r_y)
        #         v_x = r_x  # v hat gleiche Richtung wie r
        #         v_y = r_y
        #         v_z = r_z
        #
        #     self.bodies.append(Particle(
        #         i,
        #         r_x * r,
        #         r_y * r,
        #         r_z * r,
        #         v_x * v,  # Vorzeichen von r_x, Größe von r_x und gaußsches Rauschen
        #         v_y * v,
        #         v_z * v,
        #         mass,
        #         color[i % 13],
        #         self.timesteps))


    def calc_force(self) -> List:
        '''
        Calculate force for all bodies in Cluster and checks for collisions.

        Args: 
            None

        Returns: List (of all colliding particles)
        '''
        G = 6.67e-11  # Gravitationskonstante
        rho = 3000  # Dichte [kg/m^3]    mass = 4/3 pi r^3 * rho
        collisions = [[] for body in self.bodies]  # Erstellung einer Liste für die Kollisionspartner
        # Berechnung der Gravitationskräfte für jeden Körper
        for body in self.bodies:
            if body.mass != 0:
                temp = []
                r_body = (body.mass * 3 / (4 * np.pi * rho)) ** (1/3)  # Radius des Körpers
                body.force_x = 0
                body.force_y = 0
                body.force_z = 0
                # Stoßbedingung: Für jeden Körper wird geschaut, ob sie sich berühren
                for neighbor in self.bodies:
                    r_neighbor = (body.mass * 3 / (4 * np.pi * rho)) ** (1 / 3)  # Radius des Nachbars

                    if neighbor != body and neighbor.mass != 0:
                        # Distanzberechnung zwischen den Körpern
                        distance = ((body.pos_x - neighbor.pos_x) ** 2 + (body.pos_y - neighbor.pos_y) ** 2 + (body.pos_z - neighbor.pos_z) ** 2)
                        # Wenn die Distanz kleiner ist, als die gemeinsamen Radien der Körper, dann stoßen sie
                        if distance < (r_body + r_neighbor):
                            temp.append(neighbor.name)

                        # Berechnung der Gravitationskraft der Körper
                        force = G * body.mass * neighbor.mass / distance
                        direction = (-(body.pos_x - neighbor.pos_x), -(body.pos_y - neighbor.pos_y),
                                    -(body.pos_z - neighbor.pos_z))
                        if direction[1] and direction[0] != 0:
                            alpha = direction[0] / direction[1]
                        else:
                            alpha = 0.001
                        if direction[2] and direction[0] != 0:
                            beta = direction[0] / direction[2]
                        else:
                            beta = 0.001
                        if direction[1] and direction[2] != 0:
                            gamma = direction[1] / direction[2]
                        else:
                            gamma = 0.001
                        body.force_x += np.sign(direction[0]) * force / sqrt(1 + 1 / (alpha ** 2) + 1 / (beta ** 2))
                        body.force_y += np.sign(direction[1]) * force / sqrt(1 + alpha ** 2 + 1 / (gamma ** 2))
                        body.force_z += np.sign(direction[2]) * force / sqrt(1 + beta ** 2 + gamma ** 2)
                collisions[body.name] = temp
        return collisions
    


    def calc_velocity(self, dt) -> None:
        '''
        Calculates new velocity after timestep for every body in Cluster.
        
        Args:
            dt: timestep of simulation [s]
        
        Returns: None
        '''
        for body in self.bodies:
            if body.mass != 0:
                body.vel_x += (body.force_x/body.mass) * dt
                body.vel_y += (body.force_y/body.mass) * dt
                body.vel_z += (body.force_z/body.mass) * dt
            else:
                body.vel_x = 0
                body.vel_y = 0
                body.vel_z = 0


    def calc_position(self, dt) -> None:
        '''
        Calculates new position of every body in Cluster. 
        
        Args:
            dt: timestep of simulation [s]
            
        Returns: 
            None
        '''
        for body in self.bodies:
            body.pos_x += body.vel_x * dt
            body.pos_y += body.vel_y * dt
            body.pos_z += body.vel_z * dt


    @timer
    def simulation(self, dt) -> List:
        '''Simulates gravitational events for all timesteps by using `calc_force`, `calc_vel` and `calc_pos`. Sets mass of lighter body to zero when colliding.
        
        Args:
            dt: timestep of simulation [s]
            
        Returns:
            None
        '''
        for t in range(self.timesteps):
            collisions = self.calc_force()
            self.calc_velocity(dt)
            dealt_elements = tuple()
            for body in self.bodies:
                if body.mass != 0:
                    body.pos_x_history[t] = body.pos_x
                    body.pos_y_history[t] = body.pos_y
                    body.pos_z_history[t] = body.pos_z
                else:
                    body.pos_x_history[t] = None
                    body.pos_y_history[t] = None
                    body.pos_z_history[t] = None
                if body.name not in dealt_elements:
                    if len(collisions[body.name]) != 0 and body.mass != 0:
                        for i in collisions[body.name]:
                            self.bodies[body.name].mass += self.bodies[i].mass
                            dealt_elements += (i,)
                            self.bodies[i].mass = 0
            self.calc_position(dt)
            self.all_collisions.append(collisions)


    def plot2d(self, renderpoints=500) -> None:
        '''Plots trajectories of all bodies for all timesteps.
        
        Args:
            renderpoints=500: Number of points to plot for every trajectory
            
        Returns:
            None
        '''
        plt.figure()
        for body in self.bodies:
            plt.plot(body.pos_x_history, 
                    body.pos_y_history, 
                    color=body.color)
        plt.xlabel('x [m]', fontsize=16)
        plt.ylabel('y [m]', fontsize=16)
        plt.tick_params(axis='x', labelsize=16)
        plt.tick_params(axis='y', labelsize=16)
        plt.show()

    def plot3d(self, renderpoints=500) -> None:
        '''Plots trajectories of all bodies for all timesteps.

        Args:
            renderpoints=500: Number of points to plot for every trajectory

        Returns:
            None
        '''
        plt.figure()
        ax = plt.axes(projection='3d')
        for body in self.bodies:
            ax.plot3D(body.pos_x_history,
                     body.pos_y_history, body.pos_z_history,
                     color=body.color)
        ax.set_xlabel('x [m]')
        ax.set_ylabel('y [m]')
        ax.set_zlabel('z [m]')
        ax.set_xlim3d(-30000,70000)
        ax.set_ylim3d(-100000,100000)
        ax.set_zlim3d(-100000,100000)
        plt.show()

    def _return_history(self) -> None:
        for i in self.bodies:
            pprint((i.pos_x_history, i.pos_y_history))


    def _return_collisions(self) -> None:
        count = 0
        for t in self.all_collisions:
            for body in t:
                if len(body) != 0:
                    count += 1 
        print('number of particles involved in collisions: ' + str(count))
        

    @timer
    def animation2d(self) -> None:
        '''Animates the simulation by plotting coordinates of all bodies for every timestep.

        Args:
            None

        Returns:
            None
        '''
        plt.figure()
        for t in range(self.timesteps):
            for body in self.bodies:
                if body.pos_x_history[t] != None:
                    plt.scatter(body.pos_x_history[t], body.pos_y_history[t], color=body.color)
            plt.pause(0.00000001)
            # plt.cla()
            # plt.axis([-5000, 5000, -5000, 5000])




# Initialization, simulation and plotting

solarsystem = Cluster(timesteps=int(365))
solarsystem.create_bodies(num_bodies=30, min_mass=10 ** 2, max_mass=10 ** 7, max_radius=10 ** 4, max_vel=10 ** (-5), s='3D')
solarsystem.simulation(dt=86400 / 24)
solarsystem.plot2d()
solarsystem.plot3d()
solarsystem._return_collisions()
# solarsystem.animation2d()
# solarsystem._return_collisions()