from cmath import pi
import numpy as np
import matplotlib.pyplot as plt
import csv
import scipy.integrate as integrate
import os


# Einlesen der Daten
def read_data(file):
    with open(file, 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=";")
        next(csvreader)
        depth = []
        rho = []
        vp = []
        vs = []
        for row in csvreader:
            if row:
                depth.append(float(row[0]))  # km
                rho.append(float(row[1]) * 1000)  # kg/m^3
                vp.append(float(row[2]))  # km/s
                vs.append(float(row[3]))  # km/s
    return np.column_stack([depth]), np.column_stack([rho]), np.column_stack([vp]), np.column_stack([vs])


def mass(rho, r):
    '''Calculates mass of earth inside sphere of radius r.'''
    return (4/3)*pi*rho*r**3


# Berechnung der Dichte
def density_expansion(rho_old, vp, vs, r_new, r_old):
    '''Returns Euler approximation for rho at r2.
    Method: 
    rho(r+h) = rho(r) + h*d(rho)/dr(r)'''
    G = 6.67 * 10**(-11)  # Gravitationskonstante
    phi = vp**2 - (4/3) * vs**2
    rho_new = rho_old + (r_new - r_old) * (-G * mass(r_old, rho_old) * rho_old / (r_old**2 * phi))
    return rho_new


file = os.getcwd() + r"\VpVs-valuesak135.csv"   

# Einlesen der Daten
depth, density, vp, vs = read_data(file)  # Tiefe, Dichte, vp, vs

# r geht vom Mittelpunkt nach außen
r = 6371 - depth

# Startbedingungen
rho_0 = 3000  # kg/m^3
M_0 = 5.973 * 10**24  # Masse in 18km Tiefe

rho = np.zeros(len(density))
rho[:9] = rho_0

for i in range(9,len(rho)):
    rho[i] = density_expansion(rho[i-1], vp[i-1], vs[i-1],r[i], r[i-1])


def corrected_mass(rho, r):
    res = 0
    for i in range(len(rho)):
        res += (4/3)*pi*rho[i]*(r[i]**3 - r[i-1]**3)
    return res

print(corrected_mass(rho,r))

plt.plot(rho, r)
plt.plot(density, r)
plt.show()

# Berechnung von rho und M für jede Schicht
# for i in range(8, len(rho) - 1):  # Die ersten Werte werden weg gelassen
#     if r[i, 0] != r[i-1, 0]:
#         M[i+1] = mass(r[i, 0], r[i-1, 0], rho[i])
#         rho[i+1] = density(M[i+1], rho[i], vp[i, 0], vs[i, 0], r[i, 0], r[i-1, 0])
#     else:
#         M[i+1] = M[i]
#         rho[i+1] = rho[i]