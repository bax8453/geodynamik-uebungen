from cmath import pi
import numpy as np
import matplotlib.pyplot as plt
import csv
import scipy.integrate as integrate
import os


def read_data(file) -> tuple:
    '''Open .csv file and returns tuple of 4 lists containing depth, density, p-wave velocity and s-wave velocity.'''
    with open(file, 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=";")
        next(csvreader)
        depth = []
        rho = []
        vp = []
        vs = []
        for row in csvreader:
            if row:
                depth.append(float(row[0]))  # km
                rho.append(float(row[1]) * 1000)  # kg/m³
                vp.append(float(row[2]))  # km/s
                vs.append(float(row[3]))  # km/s
    return np.column_stack([depth]), np.column_stack([rho]), np.column_stack([vp]), np.column_stack([vs])

def mass(r: list, rho_r: list) -> float:
    '''Calculates difference of actual mass of earth and mass calculated by a given density model.
    If density model is not complete, the function returns the mass not yet incorporated in the model.'''
    M_e=5.951e24
    result = M_e
    for i in range(len(r)-1):
        result -= (4/3)*np.pi*rho_r[i]*(r[i]**3-r[i+1]**3)
    return result

def density_expansion_explicit(rho: list, vp: list, vs: list, r: list, i: int) -> list:
    '''Returns explicit Euler approximation for rho at index i using rho[i-1].
    Method: 
    rho(r+h) = rho(r) + h*d(rho)/dr(r)'''
    G = 6.67 * 10**(-11)  # Gravitationskonstante
    phi = vp[i-1]**2 - (4/3) * vs[i-1]**2
    rho[i] = rho[i-1] + (r[i]-r[i-1]) * (-G * mass(r, rho) * rho[i-1] / (r[i-1]**2*phi))
    return rho

def density_expansion_implicit(rho, vp, vs, r, i):
    '''Returns implicit Euler approximation (Heun method) for rho at index i using explicit approx. for drho/dr(r[i]).
    Method: 
    rho(r+h) = rho(r) + h*d(rho)/dr(r+1) with d(rho)/dr(r+1) = d(rho)/dr(rho(r) + h*d(rho)/dr(r))'''
    G = 6.67 * 10**(-11)  # Gravitationskonstante
    phi = vp[i-1]**2 - (4/3) * vs[i-1]**2
    rho[i] = rho[i-1] + (r[i]-r[i-1]) * (-G * mass(r, density_expansion_explicit(rho, vp, vs, r, i)) * density_expansion_explicit(rho, vp, vs, r, i)[i] / (r[i]**2*phi))
    return rho

def mass_of_shells(r: list, rho: list) -> list:
    '''Returns a list of masses of all layers described by the density model.'''
    mass = np.zeros(len(r))
    for i in range(len(r)-1):
        mass[i] = (4/3)*np.pi*rho[i]*(r[i]**3-r[i+1]**3)
    return mass

def moment_of_inertia(r: list, M: list) -> float:
    '''Calulates moment of inertia for model of sphere.'''
    J = np.zeros(len(r)-1)
    for i in range(len(r)-1):
        if r[i] != r[i+1]:
            J[i] = 2/5 * M[i] *(r[i]**5 - r[i+1]**5)/(r[i]**3 - r[i+1]**3)
    return sum(J)

def calc_density(rho_0: int, rho_CMB: int, radius: list, vp: list, vs: list) -> list:
    '''Calculate density profile for given boundary conditions.'''
    rho_ex = np.zeros(len(radius))
    rho_im = np.zeros(len(radius))
    rho_ex[8] = rho_0
    rho_im[8] = rho_0
    depth = 6371 - (radius / 1000)
    for i in range(9, len(radius)):
        if depth[i] == 2891.5 and depth[i-1] ==2891.5: # CMB
            # rho_ex[i] = 9971.42857142857
            rho_ex[i] = rho_CMB
            rho_im[i] = 9500
        elif depth[i] == 5153.5 and depth[i-1] == 5153.5: # ICB
            rho_ex[i] = 12700

        # more discontinuities
        # elif depth[i] == 410 and depth[i-1] == 410:   # 410km discontinuity
        #    rho_ex[i] = 3931.7
        # elif depth[i] == 660 and depth[i-1] == 660:   # 660km discontinuity
        #    rho_ex[i] = 4238.7
        # elif depth[i] == 2740 and depth[i-1] == 2740:   # D" discontinuity
        #    rho_ex[i] = 5693.4 
        else:
            rho_ex = density_expansion_explicit(rho_ex, vp, vs, radius, i)
            # rho_im = density_expansion_implicit(rho_im, vp, vs, r, i)
    return rho_ex

def best_inertia_model(file, n=31, rho_0_low=3600, rho_0_high=3700, rho_CMB_low=9450, rho_CMB_high=9950):  
    depth, density, vp, vs = read_data(file)

    M_0 = 5.951e24  # Masse in 18km Tiefe
    I_0 = 7.964e37  # Moment of Inertia, without the uppermost 18km
    vp *= 1000
    vs *= 1000

    radius_model = (6371 - depth) * 1000

    rho_0_variant = np.linspace(rho_0_low,rho_0_high,n)
    rho_CMB_variant = np.linspace(rho_CMB_low,rho_CMB_high,n)

    density_profiles = [[[] for i in range(len(rho_CMB_variant))] for j in range(len(rho_0_variant))]
    mass_of_profiles = np.array([[0.0 for i in range(len(rho_CMB_variant))] for j in range(len(rho_0_variant))])
    inertia_of_profiles = np.array([[0.0 for i in range(len(rho_CMB_variant))] for j in range(len(rho_0_variant))])

    for i in range(len(density_profiles)):
        for j in range(len(density_profiles[i])):
            density_profiles[i][j] = calc_density(rho_0_variant[i], rho_CMB_variant[j], radius_model, vp, vs)
            mass_of_profiles[i][j] = sum(mass_of_shells(radius_model, density_profiles[i][j]))
            inertia_of_profiles[i][j] = moment_of_inertia(radius_model, mass_of_shells(radius_model, density_profiles[i][j]))

    inertia_choice = np.min(np.array(abs(inertia_of_profiles - I_0)))
    # print(np.array(abs(inertia_of_profiles - I_0)))
    inertia_choice_i = np.unravel_index(np.array(abs(inertia_of_profiles - I_0)).argmin(), inertia_of_profiles.shape)
    print('Index of choice (minimum inertia deficit): ', inertia_choice_i)

    density_choice = density_profiles[inertia_choice_i[0]][inertia_choice_i[1]]
    mass_choice = mass_of_profiles[inertia_choice_i[0]][inertia_choice_i[1]]
    print('Difference to real values (mass/inertia): ', abs(mass_choice - M_0), inertia_choice)

    print('Absolute optimal values (mass/inertia): ', mass_choice, inertia_of_profiles[inertia_choice_i[0]][inertia_choice_i[1]])
    print('Boundary conditions for optimum (r=0/r=CMB): ', rho_0_variant[inertia_choice_i[0]], rho_CMB_variant[inertia_choice_i[1]])

    plt.figure()
    plt.plot(density_choice,radius_model, label='calculated density profile')
    plt.plot(density,radius_model, label= 'PREM: density profile')
    plt.xlabel('density [kg/m³]', fontsize=20)
    plt.ylabel('radius [m]', fontsize=20)
    plt.legend(fontsize=20)
    plt.show()

def best_mass_model(file, n=31, rho_0_low=3700, rho_0_high=3900, rho_CMB_low=9950, rho_CMB_high=10050):
    depth, density, vp, vs = read_data(file)

    M_0 = 5.951e24  # Masse in 18km Tiefe
    I_0 = 7.964e37  # Moment of Inertia, without the uppermost 18km
    vp *= 1000
    vs *= 1000

    radius_model = (6371 - depth) * 1000

    rho_0_variant = np.linspace(rho_0_low,rho_0_high,n)
    rho_CMB_variant = np.linspace(rho_CMB_low,rho_CMB_high,n)

    density_profiles = [[[] for i in range(len(rho_CMB_variant))] for j in range(len(rho_0_variant))]
    mass_of_profiles = np.array([[0.0 for i in range(len(rho_CMB_variant))] for j in range(len(rho_0_variant))])
    inertia_of_profiles = np.array([[0.0 for i in range(len(rho_CMB_variant))] for j in range(len(rho_0_variant))])

    for i in range(len(density_profiles)):
        for j in range(len(density_profiles[i])):
            density_profiles[i][j] = calc_density(rho_0_variant[i], rho_CMB_variant[j], radius_model, vp, vs)
            mass_of_profiles[i][j] = sum(mass_of_shells(radius_model, density_profiles[i][j]))
            inertia_of_profiles[i][j] = moment_of_inertia(radius_model, mass_of_shells(radius_model, density_profiles[i][j]))

    mass_choice = np.min(np.array(abs(mass_of_profiles - M_0)))
    # print(np.array(abs(mass_of_profiles - M_0)))
    mass_choice_i = np.unravel_index(np.array(abs(mass_of_profiles - M_0)).argmin(), mass_of_profiles.shape)
    print('Index of choice (minimum mass deficit): ', mass_choice_i)

    density_choice = density_profiles[mass_choice_i[0]][mass_choice_i[1]]
    inertia_choice = inertia_of_profiles[mass_choice_i[0]][mass_choice_i[1]]
    print('Difference to real values (mass/inertia): ', mass_choice, abs(inertia_choice - I_0))

    print('Absolute optimal values (mass/inertia): ', mass_of_profiles[mass_choice_i[0]][mass_choice_i[1]], inertia_choice)
    print('Boundary conditions for optimum (r=0/r=CMB): ', rho_0_variant[mass_choice_i[0]], rho_CMB_variant[mass_choice_i[1]])

    plt.figure()
    plt.plot(density_choice,radius_model, label='Calculated density profile')
    plt.plot(density,radius_model, label='PREM: density profile')
    plt.xlabel('density [kg/m³]', fontsize=20)
    plt.ylabel('radius [m]', fontsize=20)
    plt.legend(fontsize=20)
    plt.show()




file = os.getcwd() + r"\VpVs-valuesak135.csv"   

print('Calculating optimal density model in regard to total mass. Depending on the grid size of the search domain this may take a while. \n')

best_mass_model(file,n=20,rho_0_low=3660,rho_0_high=3730)

print('\nCalculating optimal density model in regard to inertia. Depending on the grid size of the search domain this may take a while. \n')

best_inertia_model(file,n=15)

