from typing import List, Tuple

import numpy as np
from numpy import sqrt, sign
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
from time import time
from mpl_toolkits import mplot3d

start_programm=time()

'''
Aufgabenstellung:
Simulation der Bahnbewegungen aller 8 im Sonnensystem befindlichen Planeten in 2D inklusive ein paar Monden.
'''

# Das Programm ist objektorientiert geschrieben, daher hier zunächst die Init-Funktion der Klasse.
# Außerdem werden Funktionen geschrieben zum Zurückgeben der Kraft, Position, Geschwindigkeit und Beschleunigung.
class Particle():
    def __init__(self, name, pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, mass, color) -> None:
        self.name = name
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.pos_z = pos_z
        self.vel_x = vel_x
        self.vel_y = vel_y
        self.vel_z = vel_z
        self.mass = mass
        self.force_x = 0.0
        self.force_y = 0.0
        self.force_z = 0.0
        self.color = color
        self.pos_x_history = [pos_x]
        self.pos_y_history = [pos_y]
        self.pos_z_history = [pos_z]

    # Rückgabe Kraft in x-, y- und z- Richtung
    def get_force(self) -> Tuple:
        return (self.force_x, self.force_y, self.force_z)

    # Rückgabe Position in x-, y- und z- Richtung
    def get_position(self) -> Tuple:
        return (self.pos_x, self.pos_y, self.pos_z)

    # Rückgabe Geschwindigkeit in x-, y- und z- Richtung
    def get_velocity(self) -> Tuple:
        return (self.vel_x, self.vel_y, self.vel_z)

    # Rückgabe Beschleunigung in x-, y- und z- Richtung
    def get_acceleration(self) -> Tuple:
        return (self.force_x/self.mass, self.force_y/self.mass, self.force_z/self.mass)


# Berechnung der gesamt wirkenden Kraft auf einen Körper mittels Gravitationsgesetz:
def calc_force(bodies) -> None:
    G = 6.67e-11  # Gravitationskonstante, N · m² / kg²
    for body in bodies:
        # Anfangskraft auf Körper = 0 (Inertialisierungsschritt)
        body.force_x = 0
        body.force_y = 0
        body.force_z = 0

        # Jeder Körper übt eine Gravitationskraft auf seine betrachteten Körper aus
        for neighbor in bodies:
            if neighbor != body:  # Für jeden anderen Körper, der nicht der betrachtete Körper selber ist
                # Gesamt ausgeübte Gravitationskraft der Nachbarkörper auf den betrachteten Körper
                force = G * body.mass * neighbor.mass / ((body.pos_x - neighbor.pos_x) ** 2 +
                                                           (body.pos_y - neighbor.pos_y) ** 2 +
                                                           (body.pos_z - neighbor.pos_z) ** 2)
                # Richtung der Kraft in x-, y- und z- Richtung
                direction = (-(body.pos_x - neighbor.pos_x), -(body.pos_y - neighbor.pos_y),
                             -(body.pos_z - neighbor.pos_z))
                # Folgende Berechnung notwendig zur Zerlegung der Gesamtkraft in ihre Komponenten. Ausnahme bei Nullteilerdivision vorübergehend. Wird noch behoben.
                if direction[1] and direction[0] != 0:
                    alpha = direction[0] / direction[1]
                else:
                    alpha = 0.001
                if direction[2] and direction[0] != 0:
                    beta = direction[0] / direction[2]
                else:
                    beta = 0.001
                if direction[1] and direction[2] != 0:
                    gamma = direction[1] / direction[2]
                else:
                    gamma = 0.001


                # Berechnung der Kraft in x-, y- und z- Richtung
                body.force_x += np.sign(direction[0]) * force / sqrt(1 + 1 / (alpha ** 2) + 1 / (beta ** 2))
                body.force_y += np.sign(direction[1]) * force / sqrt(1 + alpha ** 2 + 1 / (gamma ** 2))
                body.force_z += np.sign(direction[2]) * force / sqrt(1 + beta ** 2 + gamma ** 2)


# Funktion zum Berechnen der Geschwindigkeit in x-, y- und z-Richtung.
# Zum Berechnen der neuen Geschwindigkeit v in jedem Zeitschritt dt wird benutzt, dass
# F = m*a bzw a = F/m und a = v*dt gilt, also v = (F/m)*dt. Jede Kraftänderung führt zu einer Geschwindigkeitsänderung,
# welche die vorherige Geschwindigkeit beeinflusst. Daher wird die neue Geschwindigkeit auf die Alte drauf gerechnet. 
# Diese Gleichungen sind selbstverständlich nur Näherungen. Für kleine Zeitschritte ist der Fehler aber verschwindet gering,
# sodass die Simulation ein sinnvolles Ergebis erreicht.
def calc_velocity(objects, dt) -> None:
    for object in objects:
        object.vel_x += (object.force_x/object.mass) * dt
        object.vel_y += (object.force_y/object.mass) * dt
        object.vel_z += (object.force_z/object.mass) * dt


# Außerdem wird noch die Position nach jedem Zeitschritt dt benötigt. Dabei wird ausgenutzt, dass sich die
# Strecke pos in jedem Zeitschritt dt näherungsweise linear ändert mit pos = v*dt. Dies gilt erst recht nicht mehr, wenn
# zwei Körper sehr dicht beieinander sind. In dem aktuellen Problem betrachten wir aber die Bewegung der Planeten
# um die Sonne. Wenn man den Zeitschritt dt also hinreichend klein wählt, kann man eine lineare Änderung der Strecke
# annehmen, da sich zwei Planeten nicht allzu dicht kommen.
# Jede Positionsänderung wird auf die vorherige Position draufgerechnet.

# Funktion zum Berechnen der Position in x-, y- und z- Richtung.
def calc_position(objects, dt) -> None:
    for object in objects:
        object.pos_x += object.vel_x * dt
        object.pos_y += object.vel_y * dt
        object.pos_z += object.vel_z * dt


# Mit der Funktion simulation() wird die Simulation durchgeführt. Für jeden Körper wird zu jedem Zeitschritt
# die gesamtwirkende Kraft und die daraus resultierende Positionsänderung berechnet.
# Die Liste memory speichert die Positionen aller Körper. Dazu werden die Positionen aller Körper aber auch direkt in
# der Instanz des Particle gespeichert. 
def simulation(objects, dt, timesteps) -> List:
    memory = [[] for object in objects]
    for t in range(timesteps):
        start_force = time()
        calc_force(objects)
        end_force = time()

        start_vel = time()
        calc_velocity(objects, dt)
        end_vel = time()

        start_pos = time()
        calc_position(objects, dt)
        end_pos = time()

        for object in objects:
            object.pos_x_history.append(object.pos_x)
            object.pos_y_history.append(object.pos_x)
            object.pos_z_history.append(object.pos_x)

        # Memory Liste veraltet. Es werden die Positionen aus den Particle-Instanzen verwendet.
        for i in range(len(objects)):
            memory[i].append(objects[i].get_position())

    return memory, start_force, end_force, start_vel, end_vel, start_pos, end_pos


# Mit der Funktion plotting3D() werden die Positionen in 3D geplottet.
def plotting3D(data, string, legend, render_points, answ='no') -> None:
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    for object in data:
        ax.plot([i[0] for i in object[::len(object)//render_points]],
                [i[1] for i in object[::len(object)//render_points]],
                [i[2] for i in object[::len(object)//render_points]])
    plt.xlabel('x Position [m]', fontsize=14)
    plt.xticks(fontsize=14)
    plt.ylabel('y Position [m]', fontsize=14)
    plt.yticks(fontsize=14)
    ax.set_zlabel('z Position [m]', fontsize=14)
    ax.tick_params(axis='z', labelsize=14)
    plt.title(string + '3D', fontsize=16, fontweight='bold')
    plt.legend(legend)
    ax.set_zlim([-4.5e12, 4.5e12])
    if answ != 'no':
        plt.axis([-0.025e11, 0.025e11, -0.025e11, 0.025e11])
    plt.show()

# Mit der Funktion plotting2D() werden die Positionen in 2D geplottet.
def plotting2D(data, string, legend, render_points, answ='no') -> None:
    for object in data:
        plt.plot([i[0] for i in object[::len(object) // render_points]],
                 [i[1] for i in object[::len(object) // render_points]])
    plt.xlabel('x Position [m]', fontsize=14)
    plt.xticks(fontsize=14)
    plt.ylabel('y Position [m]', fontsize=14)
    plt.yticks(fontsize=14)
    plt.title(string + '2D', fontsize=16, fontweight='bold')
    plt.legend(legend)
    if answ != 'no':
        plt.axis([-0.025e11, 0.025e11, -0.025e11, 0.025e11])
    plt.show()


# Das Programm ist objektorientiert geschrieben. Jeder Körper der Simulation ist Instanz der particle-Klasse. In der Klasse ist Name,
# Position, Geschwindigkeit und Masse festgelegt. Außerdem wird eine Farbe zum darstellen zugeordnet.

# Particle(name, pos_x [m], pos_y [m], pos_z [m], vel_x [m/s], vel_y [m/s], vel_z [m/s], mass [kg], colour)
if __name__ == '__main__':

    particles = [Particle("Sonne",      0,              0,                      0,                  0,
                          0,          0,      1.99e30,    'orange'),
                 Particle("Merkur",     0.57e11,        1,                      7e9,                0,
                          4.74e4,     0,      0.33e24,    'brown'),
                 Particle('Venus',      1.08e11,        2,                      6.4e9,              0,
                          3.5e4,      0,      4.875e24,   'magenta'),
                 Particle("Erde",       1.496e11,       3,                      1,                  0,
                          3e4,        0,      5.97e24,    'blue'),
                 Particle("Mars",       2.28e11,        4,                      7.2e9,              0,
                          2.41e4,     0,      0.642e24,   'red'),
                 Particle("Jupiter",    0,              7.78e11,                0,                  -1.303e4,
                          0,          0,      1.898e27,   'cyan'),
                 Particle("Saturn",     14.32e11,       5,                      6.25e10,            0,
                          0.97e4,     0,      568e24,     'yellow'),
                 Particle("Uranus",     28.67e11,       6,                      3.9e10,             0,
                          0.68e4,     0,      86.8e24,    'pink'),
                 Particle("Neptun",     45.15e11,       7,                      1.4e11,             0,
                          0.54e4,     0,      102e24,     'grey'),
                 Particle("Mun",        1.496e11+3.829e8, 8,                    3.4e7,              0,
                          3e4+0.107e4,        0,      7.346e22,    'gray'),
                 Particle("Io",         -4.21e8,       7.78e11+1,              1,                 -1.303e4,
                          1.7e4,      0,      8.93e22,    'violet'),
                 Particle("Europa",     6.71e8,        7.78e11+2,              2,                 -1.303e4,
                          -1.3e4,     0,      4.8e22,     'bordeaux'),
                 Particle("Ganymede",   0,             7.78e11-1.07e9,         3,                 -1.303e4-1.08e4,
                          0,          0,      14.8e22,    'gray'),
                 Particle("Callisto",   0,             7.78e11+1.88e9,         4,                 -1.303e4+0.82e4,
                          0,          0,      10.8e22,    'green')]

    # Ekliptikwerte:
    # Merkur: 7°
    # Venus: 3,4°
    # Erde: 0°
    # Mond: 5,1°
    # Mars: 1,8°
    # Jupiter: 1,3° --> z_max = 1.5e10m --> v_z = cos(90-1.3)*v_ges, v_x = -1.3026e4, v_z = -0.296e4
    # Saturn: 2,5°
    # Uranus: 0,8°
    # Neptun: 1,8°


    # Zeitschrittgröße dt
    # 24h = 86400s -> gut für Planeten, aber nicht für Mond
    # Der Mond braucht 27,3 Tage um Erde
    # Insgesamt betrachtete Zeit -> 10 Jahre: 365 * 10

    dt = 86400 / 12
    # dt = 3600  # 1h

    # Insgesamt betrachtete Zeit -> 10 Jahre: 365 * 10
    timesteps = 365 * 10 * 12

    timesteps = 365 * 12 * 10
    # timesteps = 365 * 24 * 4

    render_points = timesteps

    
    # Durchführung der Simulation
    start_simulation=time()
    data, start_force, end_force, start_vel, end_vel, start_pos, end_pos = simulation(particles, dt, timesteps)
    end_simulation=time()

    
    # Plotten der Daten in 2D
    # plotting2D(data, 'Umlaufbahnen', legend=[i.name for i in particles], render_points=min(timesteps, render_points))
    end_plotten2d = time.time()
    start_plot2d = time()
    plotting2D(data, 'Umlaufbahnen', legend=[i.name for i in particles], render_points=min(timesteps, render_points))
    end_plot2d = time()


    
    # Plotten der Daten in 3D
    # plotting3D(data, 'Umlaufbahnen', legend=[i.name for i in particles], render_points=min(timesteps, render_points))
    end_plotten3d = time.time()
    start_plot3d = time()
    plotting3D(data, 'Umlaufbahnen', legend=[i.name for i in particles], render_points=min(timesteps, render_points))
    end_plot3d = time()


    # Animation
    # Für die Animation werden Funktionen benötigt, welche die Position für jeden Zeitschritt an die Funktion
    # FuncAnimation übergeben. Außerdem werden die Achsen angepasst für verschiedene Zeitpunkte für verschiedene
    # Ausschnitte

    color = ['orange', 'brown', 'magenta', 'blue', 'red', 'cyan', 'yellow', 'pink', 'blue', 'gray', 'violet', 'black',
             'grey', 'green']

    start_animation = time()
    fig, ax = plt.subplots(figsize=(20, 15))

    ax.set_xlim([- 45.16e11, 45.16e11])
    ax.set_ylim([- 45.16e11, 45.16e11])

    for i in range(len(data)):
        globals()[f"line_{i}"], = ax.plot([], [], marker='o', color=color[i])

    def init_func():
        ax.set_xlabel('x Position [m]', fontsize=14)
        ax.set_ylabel('y Position [m]', fontsize=14)
        plt.xticks(fontsize=14)
        plt.yticks(fontsize=14)
        plt.legend([i.name for i in particles], loc='lower left')

    def update_plot(j):
        for i in range(len(data)):
            globals()[f"line_{i}"].set_data(data[i][j][0], data[i][j][1])

        plt.title('Full Solar System, Day ' + str(int(j * dt / 86400)), fontsize=16, fontweight='bold')
        plt.axis(
            [data[0][j][0] - 45.16e11, data[0][j][0] + 45.16e11, data[0][j][1] - 45.16e11,
             data[0][j][1] + 45.16e11])

        if 20 <= int(j * dt / 86400) < 30:
            plt.title('Inner Solar System, Day ' + str(int(j * dt / 86400)), fontsize=16, fontweight='bold')
            plt.axis([data[0][j][0] - 1.5e11, data[0][j][0] + 1.5e11, data[0][j][1] - 1.5e11,
                      data[0][j][1] + 1.5e11])

        if 30 <= int(j * dt / 86400) < 50:
            plt.title('Earth and Moon, Day ' + str(int(j * dt / 86400)), fontsize=16, fontweight='bold')
            plt.axis([data[3][j][0] - 9e8, data[3][j][0] + 9e8, data[3][j][1] - 9e8,
                      data[3][j][1] + 9e8])

        if 70 <= int(j * dt / 86400) < 90:
            plt.title('Jupiter and Moons, Day ' + str(int(j * dt / 86400)), fontsize=16, fontweight='bold')
            plt.axis([data[5][j][0] - 2e9, data[5][j][0] + 2e9, data[5][j][1] - 2e9,
                      data[5][j][1] + 2e9])

        return line_6,

    # Animationsfunktion
    anim = FuncAnimation(fig, update_plot, init_func=init_func, frames=20000, interval=20)

    plt.show()
    end_animation=time()
end_programm=time()


# Für Laufzeitverhalten
print('Gesamtzeit: {:5.3f}s'.format(end_programm - start_programm))
print('Simulation: {:5.3f}s'.format(end_simulation - start_simulation))
print('Plotten 2D: {:5.3f}s'.format(end_plot2d - start_plot2d))
print('Plotten 3D: {:5.3f}s'.format(end_plot3d - start_plot3d))
print('Animation: {:5.3f}s'.format(end_animation - start_animation))
print('Kraft: {:5.3f}s'.format(end_force - start_force))
print('Vel: {:5.3f}s'.format(end_vel - start_vel))
print('Pos: {:5.3f}s'.format(end_pos - start_pos))
#print('Plot 3D: {:5.3f}s'.format(end_plt3D-st_plt3D))






'''
if __name__ == '__main__':
    particles = [Particle("Sol",        0,              0,                      0,                      0,          1.99e30,    'orange'),
                 Particle("Mercurio",   0.57e11,        3,                      0,                      4.74e4,     0.33e24,    'brown'),
                 Particle('Amore',      1.08e11,        2,                      0,                      3.5e4,      4.875e24,   'magenta'),
                 Particle("Dirt",       1.496e11,       1,                      0,                      3e4,        5.97e24,    'blue'),
                 Particle("reddot",     2.28e11,        4,                      0,                      2.41e4,     0.642e24,   'red'),
                 Particle("Gas",        1,              6.64e11,                -1.303e4,               0,          1.898e27,   'cyan'),
                 Particle("Ring",       14.32e11,       5,                      0,                      0.97e4,     568e24,     'yellow'),
                 Particle("Sunaru",     28.67e11,       6,                      0,                      0.68e4,     86.8e24,    'pink'),
                 Particle("Oceanboy",   45.15e11,       7,                      0,                      0.54e4,     102e24,     'grey'),
                 Particle("Mun",        1.499e11,       8,                      0,                      3.1e4,      7.34e22,    'gray'),
                 Particle("Io",         -4.21e8,        6.64e11+1,              -1.303e4,               1.7e4,      8.93e22,    'violet'),
                 Particle("Europa",     6.71e8,         6.64e11+2,              -1.303e4,               -1.3e4,     4.8e22,     'bordeaux'),
                 Particle("Ganymede",   12,             6.64e11-1.07e9,         -1.303e4-1.08e4,        0,          14.8e22,    'gray'),
                 Particle("Callisto",   13,             6.64e11+1.88e9,         -1.303e4-0.82e4,        0,          10.8e22,    'gray')]

    # Particle("Dirt", 1.496e11, 1, 0, 3e4, 5.97e24, 'blue')
    # Particle("Gas", 6.64e14, 2, 0, 1.303e4, 1.898e27, 'cyan')
    # Particle("Mun", 1.499e11, 4, 0, 4e4, 7.34e22, 'gray')
    # Erde: 1.496e11 m ; 5.97e24 kg
    # Jupiter: 6.64e14 m ; 1.898e27 kg
    # Io: 4.21e8 m ; 8.93e22 kg
    # Europa: 6.71e8 m ; 4.8e22 kg
    # Ganymede: 1.07e9 m ; 14.8e22 kg
    # Callisto:1.88e9 m ; 10.8e22 kg
'''
