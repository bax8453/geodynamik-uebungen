from typing import Tuple
from numpy import sqrt, sign
import matplotlib.pyplot as plt
import matplotlib.animation as animation


class Particle():
    def __init__(self, name, pos_x, pos_y, vel_x, vel_y, mass, color='blue') -> None:
        self.name = name
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.vel_x = vel_x
        self.vel_y = vel_y
        self.mass = mass
        self.force_x = 0.0
        self.force_y = 0.0
        self.color = color
        self.pos_x_history = [pos_x]
        self.pos_y_history = [pos_y]

    def get_force(self) -> Tuple:
        return (self.force_x, self.force_y)

    def get_position(self) -> Tuple:
        return (self.pos_x, self.pos_y)

    def get_velocity(self) -> Tuple:
        return (self.vel_x, self.vel_y)

    def get_acceleration(self) -> Tuple:
        return (self.force_x/self.mass, self.force_y/self.mass)


def calc_force(objects) -> None:
    G = 6.67e-11
    for object in objects:
        object.force_x = 0
        object.force_y = 0
        for neighbor in objects:
            if neighbor != object:
                force = G * object.mass * neighbor.mass / ((object.pos_x - neighbor.pos_x)**2 + (object.pos_y - neighbor.pos_y)**2)
                direction = (-(object.pos_x - neighbor.pos_x), - (object.pos_y - neighbor.pos_y))
                alpha = direction[0]/direction[1]
                object.force_x += sign(-(object.pos_x - neighbor.pos_x)) * force/sqrt(1+1/(alpha**2))
                object.force_y += sign(-(object.pos_y - neighbor.pos_y)) * force/sqrt(1+alpha**2)


def calc_velocity(objects, dt) -> None:
    for object in objects:
        object.vel_x += (object.force_x/object.mass) * dt
        object.vel_y += (object.force_y/object.mass) * dt


def calc_position(objects, dt) -> None:
    for object in objects:
        object.pos_x += object.vel_x * dt
        object.pos_y += object.vel_y * dt


def simulation(objects, dt, timesteps):
    memory = [[] for object in objects]
    for t in range(timesteps):
        calc_force(objects)
        calc_velocity(objects, dt)
        calc_position(objects, dt)
        for object in objects:
            object.pos_x_history.append(object.pos_x)
            object.pos_y_history.append(object.pos_x)

        for i in range(len(objects)):
            memory[i].append(objects[i].get_position())
    return memory


def plotting(data, render_points, string, legend, answ='no'):
    plt.figure()
    for object in data:
        plt.plot([i[0] for i in object[::len(object)//render_points]], [i[1] for i in object[::len(object)//render_points]])
    plt.xlabel('x Position [m]', fontsize=14)
    plt.xticks(fontsize=14)
    plt.ylabel('y Position [m]', fontsize=14)
    plt.yticks(fontsize=14)
    plt.title(string, fontsize=16, fontweight='bold')
    plt.legend(legend)
    if answ != 'no':
        plt.axis([-0.025e11, 0.025e11, -0.025e11, 0.025e11])
    plt.show()


# Annahmen sind die Kleinwinkelnäherung, sowie x>>dx und damit verbunden die Vernachlässigung des quadratischen Terms
# im Nenner bei der Berechnung der Beschleunigungen!
def RungeKutta(objects, dt):
    for object in objects:
        calc_force(objects)
        vx1= dt * (object.force_x/object.mass)
        vy1= dt * (object.force_y/object.mass)
        x1= dt * object.vel_x
        y1= dt * object.vel_y
        vx2= dt * (object.force_x/object.mass) * (object.pos_x / (0.5 * x1 + object.pos_x))
        vy2= dt * (object.force_y/object.mass) * (object.pos_y / (0.5 * y1 + object.pos_y))
        x2= dt * (object.vel_x + 0.5 * vx1)
        y2= dt * (object.vel_y + 0.5 * vy1)
        vx3 = dt * (object.force_x/object.mass) * object.pos_x / (0.5 * x2 + object.pos_x)
        vy3 = dt * (object.force_y/object.mass) * object.pos_y / (0.5 * y2 + object.pos_y)
        x3 = dt * (object.vel_x + 0.5 * vx2)
        y3 = dt * (object.vel_y + 0.5 * vy2)
        vx4 = dt * (object.force_x / object.mass) * object.pos_x / (x3 + object.pos_x)
        vy4 = dt * (object.force_y / object.mass) * object.pos_y / (y3 + object.pos_y)
        x4 = dt * (object.vel_x + vx3)
        y4 = dt * (object.vel_y + vy3)
        object.vel_x += vx1 / 6 + vx2 / 3 + vx3 / 3 + vx4 / 6
        object.vel_y += vy1 / 6 + vy2 / 3 + vy3 / 3 + vy4 / 6
        object.pos_x += x1 / 6 + x2 / 3 + x3 / 3 + x4 / 6
        object.pos_y += y1 / 6 + y2 / 3 + y3 / 3 + y4 / 6


def simulation_rk(objects, dt, timesteps):
    memory = [[] for object in objects]
    for t in range(timesteps):
        RungeKutta(objects,dt)
        for object in objects:
            object.pos_x_history.append(object.pos_x)
            object.pos_y_history.append(object.pos_x)

        for i in range(len(objects)):
            memory[i].append(objects[i].get_position())
    return memory




if __name__ == '__main__':
    particles = [Particle("Sol", 0.1, 0.1, 0., 0., 1.99e30, 'orange'),
                 Particle("Mercurio", 0.57e11, 3., 0., 4.74e4, 0.33e24, 'brown'),
                 Particle('Amore', 1.08e11, 2., 0., 3.5e4, 4.875e24, 'magenta'),
                 Particle("Dirt", 1.496e11, 1., 0., 3.e4, 5.97e24, 'blue'),
                 Particle("reddot", 2.28e11, 4., 0, 2.41e4, 0.642e24, 'red'),
                 Particle("Gas", 1, 6.64e11, -1.303e4, 0, 1.898e27, 'cyan'),
                 Particle("Ring", 14.32e11, 5., 0., 0.97e4, 568e24, 'yellow'),
                 Particle("Sunaru", 28.67e11, 6., 0., 0.68e4, 86.8e24, 'pink'),
                 Particle("Oceanboy", 45.15e11, 7., 0., 0.54e4, 102e24, 'grey')]
    # Particle("Dirt", 1.496e11, 1, 0, 3e4, 5.97e24, 'blue')
    # Particle("Gas", 6.64e14, 2, 0, 1.303e4, 1.898e27, 'cyan')
    # Particle("Mun", 1.499e11, 4, 0, 4e4, 7.34e22, 'gray')
    # Erde: 1.496e11 m ; 5.97e24 kg
    # Jupiter: 6.64e14 m ; 1,898e27 kg


    dt = 86400
    timesteps = 365*2
    render_points = timesteps


    data = simulation_rk(particles, dt, timesteps)
    plotting(data, render_points, 'Umlaufbahnen', legend=[i.name for i in particles])




