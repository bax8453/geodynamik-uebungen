from cmath import pi
import numpy as np
import matplotlib.pyplot as plt
import csv
import scipy.integrate as integrate
import os


# Einlesen der Daten
def read_data(file):
    with open(file, 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=";")
        next(csvreader)
        depth = []
        rho = []
        vp = []
        vs = []
        for row in csvreader:
            if row:
                depth.append(float(row[0]))  # km
                rho.append(float(row[1]) * 1000)  # kg/m^3
                vp.append(float(row[2]))  # km/s
                vs.append(float(row[3]))  # km/s
    return np.column_stack([depth]), np.column_stack([rho]), np.column_stack([vp]), np.column_stack([vs])


def mass(r1, r2, rho_r, M_a):
    '''Calculates mass of earth inside sphere of radius r.'''
    return M_a- (4/3)*np.pi*rho_r*(r1**3-r2**3)


def density_expansion_explicit(rho, vp, vs, r1, r2, M_a):
    '''Returns explicit Euler approximation for rho at r2.
    Method: 
    rho(r+h) = rho(r) + h*d(rho)/dr(r)'''
    G = 6.67 * 10**(-11)  # Gravitationskonstante
    phi = vp**2 - (4/3) * vs**2
    rho_r = rho + (r2-r1) * (-G * mass(r1, r2, rho, M_a) * rho / (r1**2*phi))
    return rho_r


def density_expansion_implicit(rho, vp, vs, r1, r2, M_a):
    '''Returns implicit Euler approximation (Heun method) for rho at r2.
    Method: 
    rho(r+h) = rho(r) + h*d(rho)/dr(r+1) with d(rho)/dr(r+1) = d(rho)/dr(rho(r) + h*d(rho)/dr(r))'''
    G = 6.67 * 10**(-11)  # Gravitationskonstante
    phi = vp**2 - (4/3) * vs**2
    rho_r = rho + (r2-r1) * (-G * mass(r1, r2, density_expansion_explicit(rho, vp, vs, r1, r2, M_a), M_a) * density_expansion_explicit(rho, vp, vs, r1, r2, M_a) / (r1**2*phi))
    return rho_r

def mass_correction(rho, r):
    res = 0
    for i in range(1,len(r)):
        res += (4/3) * pi * rho[i] * -(r[i]**3-r[i-1]**3)
    return res

def moment_of_inertia(r1,r2,M):
    return 2/5 * M *(r1**5 - r2**5)/(r1**3 - r2**3)



file = os.getcwd() + r"\VpVs-valuesak135.csv"   

# Einlesen der Daten
depth, density, vp, vs = read_data(file)  # Tiefe, Dichte, vp, vs

# r geht vom Mittelpunkt nach außen
r = (6371 - depth) * 1000

# Startbedingungen
rho_0 = 3600  # kg/m^3
M_0 = 5.973 * 10**24  # Masse in 18km Tiefe

rho_ex = np.zeros(len(density))
rho_im = np.zeros(len(density))
masse = np.zeros(len(density))
masse[8] = M_0
rho_ex[8] = rho_0
rho_im[8] = rho_0

for i in range(9, len(rho_ex)):
    if depth[i] == 2891.5:
        rho_ex[i] = 9900
        rho_im[i] = 9900
        masse[i]=masse[i-1]
    else:
        rho_ex[i] = density_expansion_explicit(rho_ex[i-1], vp[i-1]*1000, vs[i-1]*1000, r[i-1], r[i], masse[i-1])
        rho_im[i] = density_expansion_implicit(rho_im[i-1], vp[i-1]*1000, vs[i-1]*1000, r[i-1], r[i], masse[i-1])
        masse[i] = mass(r[i-1], r[i], rho_ex[i-1], masse[i-1])

# Berechnung des Trägkeitsmomnts
J=np.zeros(len(masse))
for i in range(len(masse)-1):
    if r[i] !=r[i+1]:
        J[i]=moment_of_inertia(r[i],r[i+1],masse[i])
        
J[-1]=moment_of_inertia(r[i],0,masse[i])
J_ges=sum(J)

print("Earth mass explicit: ", mass_correction(rho_ex, r))
print("Earth mass implicit: ", mass_correction(rho_im, r))
print("Earth mass exact: ", mass_correction(density, r))
print(masse)
print(J_ges)


plt.plot(depth,rho_ex)
plt.plot(depth,rho_im)
plt.plot(depth,density)
plt.legend(["ex", "im", "exact"])
plt.show()

# Berechnung von rho und M für jede Schicht
# for i in range(8, len(rho) - 1):  # Die ersten Werte werden weg gelassen
#     if r[i, 0] != r[i-1, 0]:
#         M[i+1] = mass(r[i, 0], r[i-1, 0], rho[i])
#         rho[i+1] = density(M[i+1], rho[i], vp[i, 0], vs[i, 0], r[i, 0], r[i-1, 0])
#     else:
#         M[i+1] = M[i]
#         rho[i+1] = rho[i]
