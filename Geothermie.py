import numpy as np
import matplotlib.pyplot as plt

def Temperatur(T0, dT, w, k, z, t):
    T = np.ones([len(z), len(t)])
    for i in range(len(z)):
        for j in range(len(t)):
            T[i, j] = T0 + dT * np.exp(-z[i] * np.sqrt(w/(2*k))) * np.cos(w*t[j] - z[i] * np.sqrt(w/(2 * k)))

    for i in range(len(z)):
        if np.cos(-z[i] * np.sqrt(w/(2*k))) <= -0.5:
            print('Tiefe um 180° gedreht:', z[i], i)

    return T


# T berechnen für 200m Tiefe und 365 Tage
t = np.arange(0, 365, 1)

w = 7.27 * 1/400 # rad/s
T0 = 8
dT = 7
k = 1e-6
dw = np.sqrt(2*k / w)
print(dw)
z = np.linspace(0, 3*dw, 10)
T = Temperatur(T0, dT, w, k, z, t)


plt.figure()
plt.pcolor(t, z, T)
plt.xlabel('t [Tag]')
plt.ylabel('z [m]')
plt.colorbar()

plt.figure()
plt.plot(t, T[0, :], 'k')
plt.plot(t, T[9, :], 'r')

plt.show()