from tarfile import SOLARIS_XHDTYPE
from typing import List, Tuple
import numpy as np
from numpy import sqrt, sign
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
from time import time
from mpl_toolkits import mplot3d


'''
Aufgabenstellung:
Simulation der Bahnbewegungen aller 8 im Sonnensystem befindlichen Planeten in 2D inklusive ein paar Monden.
'''


class Particle():
    '''Particle Class: 
    Contains singular particles that can be used in a solar system simulation.
    Particle information consists of Position (pos), Velocity (vel), Force (force) and Mass (mass). 
    Additional features are Name and Color for plotting. 
    The positional history is tracked in the appropriate lists, three components are considered.'''

    def __init__(self, name, pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, mass, color, timessteps) -> None:
        self.name = name
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.pos_z = pos_z
        self.vel_x = vel_x
        self.vel_y = vel_y
        self.vel_z = vel_z
        self.mass = mass
        self.force_x = 0.0
        self.force_y = 0.0
        self.force_z = 0.0
        self.color = color
        self.pos_x_history = [0 if i != 0 else pos_x for i in range(timessteps)]
        self.pos_y_history = [0 if i != 0 else pos_y for i in range(timessteps)]
        self.pos_z_history = [0 if i != 0 else pos_z for i in range(timessteps)]

    def __repr__(self) -> str:
        return self.name

    # Rückgabe Kraft in x-, y- und z- Richtung
    def get_force(self) -> Tuple:
        return (self.force_x, self.force_y, self.force_z)

    # Rückgabe Position in x-, y- und z- Richtung
    def get_position(self) -> Tuple:
        return (self.pos_x, self.pos_y, self.pos_z)

    # Rückgabe Geschwindigkeit in x-, y- und z- Richtung
    def get_velocity(self) -> Tuple:
        return (self.vel_x, self.vel_y, self.vel_z)

    # Rückgabe Beschleunigung in x-, y- und z- Richtung
    def get_acceleration(self) -> Tuple:
        return (self.force_x/self.mass, self.force_y/self.mass, self.force_z/self.mass)


class Cluster():
    '''Cluster Class:
    The Cluster Class manages all necessary steps for the simulation of all particles. It is initiated with a list of all particles.
    Methods in this Class span from background simulation to on-screen plotting of trajectories. For different plots, please consider adding
    a new method. (e.g. plot3d)'''

    def __init__(self, bodies) -> None:
        self.name = 'Solar System'
        self.bodies = bodies

    def calc_force(self) -> None:
        G = 6.67e-11 
        for body in self.bodies:
            body.force_x = 0
            body.force_y = 0
            body.force_z = 0
            for neighbor in self.bodies:
                if neighbor != body:
                    force = G * body.mass * neighbor.mass / ((body.pos_x - neighbor.pos_x) ** 2 +
                                                            (body.pos_y - neighbor.pos_y) ** 2 +
                                                            (body.pos_z - neighbor.pos_z) ** 2)
                    direction = (-(body.pos_x - neighbor.pos_x), -(body.pos_y - neighbor.pos_y),
                                -(body.pos_z - neighbor.pos_z))
                    if direction[1] and direction[0] != 0:
                        alpha = direction[0] / direction[1]
                    else:
                        alpha = 0.001
                    if direction[2] and direction[0] != 0:
                        beta = direction[0] / direction[2]
                    else:
                        beta = 0.001
                    if direction[1] and direction[2] != 0:
                        gamma = direction[1] / direction[2]
                    else:
                        gamma = 0.001
                    body.force_x += np.sign(direction[0]) * force / sqrt(1 + 1 / (alpha ** 2) + 1 / (beta ** 2))
                    body.force_y += np.sign(direction[1]) * force / sqrt(1 + alpha ** 2 + 1 / (gamma ** 2))
                    body.force_z += np.sign(direction[2]) * force / sqrt(1 + beta ** 2 + gamma ** 2)
    
    def calc_velocity(self, dt) -> None:
        for body in self.bodies:
            body.vel_x += (body.force_x/body.mass) * dt
            body.vel_y += (body.force_y/body.mass) * dt
            body.vel_z += (body.force_z/body.mass) * dt

    def calc_position(self, dt) -> None:
        for body in self.bodies:
            body.pos_x += body.vel_x * dt
            body.pos_y += body.vel_y * dt
            body.pos_z += body.vel_z * dt

    def simulation(self, dt, timesteps) -> None:
        for t in range(timesteps):
            self.calc_force()
            self.calc_velocity(dt)
            self.calc_position(dt)
            for body in self.bodies:
                body.pos_x_history[t] = body.pos_x
                body.pos_y_history[t] = body.pos_y
                body.pos_z_history[t] = body.pos_z


    def plot2d(self, renderpoints):
        plt.figure()
        for body in self.bodies:
            plt.plot(body.pos_x_history[::len(body.pos_x_history)//renderpoints], body.pos_y_history[::len(body.pos_y_history)//renderpoints], color=body.color)
        plt.show()

    def plotEarth(self):
        plt.figure()
        plt.plot(self.bodies[3].pos_x_history, self.bodies[3].pos_y_history)
        plt.show()

    def _return_history(self):
        return [(i.pos_x_history, i.pos_y_history) for i in self.bodies]



if __name__ == '__main__':
    timesteps = 2000
    dt = 86400/6
    renderpoints = 501

    particles = [Particle("Sonne",      0,              0,                      0,                  0,
                            0,          0,      1.99e30,    'orange', timessteps=timesteps),
                    Particle("Merkur",     0.57e11,        1,                      7e9,                0,
                            4.74e4,     0,      0.33e24,    'brown', timessteps=timesteps),
                    Particle('Venus',      1.08e11,        2,                      6.4e9,              0,
                            3.5e4,      0,      4.875e24,   'magenta',timessteps=timesteps ),
                    Particle("Erde",       1.496e11,       3,                      1,                  0,
                            3e4,        0,      5.97e24,    'blue', timessteps=timesteps),
                    Particle("Mars",       2.28e11,        4,                      7.2e9,              0,
                            2.41e4,     0,      0.642e24,   'red', timessteps=timesteps),
                    Particle("Jupiter",    0,              6.64e11,                0,                  -1.303e4,
                            0,          0,      1.898e27,   'cyan', timessteps=timesteps),
                    Particle("Saturn",     14.32e11,       5,                      6.25e10,            0,
                            0.97e4,     0,      568e24,     'yellow', timessteps=timesteps),
                    Particle("Uranus",     28.67e11,       6,                      3.9e10,             0,
                            0.68e4,     0,      86.8e24,    'pink', timessteps=timesteps),
                    Particle("Neptun",     45.15e11,       7,                      1.4e11,             0,
                            0.54e4,     0,      102e24,     'grey', timessteps=timesteps),
                    Particle("Mun",        1.496e11+3.829e8, 8,                    3.4e7,              0,
                            3e4+0.107e4,        0,      7.346e22,    'gray', timessteps=timesteps),
                    Particle("Io",         -4.21e8,       6.64e11+1,              1,                 -1.303e4,
                            1.7e4,      0,      8.93e22,    'violet', timessteps=timesteps),
                    Particle("Europa",     6.71e8,        6.64e11+2,              2,                 -1.303e4,
                            -1.3e4,     0,      4.8e22,     'lightblue', timessteps=timesteps),
                    Particle("Ganymede",   0,             6.64e11-1.07e9,         3,                 -1.303e4-1.08e4,
                            0,          0,      14.8e22,    'gray', timessteps=timesteps),
                    Particle("Callisto",   0,             6.64e11+1.88e9,         4,                 -1.303e4+0.82e4,
                            0,          0,      10.8e22,    'green', timessteps=timesteps)]


    solarsystem = Cluster(particles)
    solarsystem.simulation(dt=dt, timesteps=timesteps)
    solarsystem.plot2d(renderpoints=renderpoints)
